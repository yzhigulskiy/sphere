provider "aws" {
  region = "us-east-1"
}

module "sg" {
  source                      = "../modules/sg"
  vpc_id                      = var.vpc_id
  subnet_cidr                 = var.subnets_cidr
}

module "ec2" {
  source                      = "../modules/ec2"
  vpc_id                      = var.vpc_id
  subnets                     = var.subnets
  keypair_private_key         = var.ssh_key
  AWS_ACCESS_KEY_ID           = var.AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY       = var.AWS_SECRET_ACCESS_KEY
}

module "alb" {
  source                      = "../modules/alb"
  subnets                     = var.subnets
  security_groups             = module.sg.sg_8080
  alb_name                    = "alb"
  vpc_id                      = var.vpc_id
  instance_id                = module.ec2.sphere_instance
}

