import boto3
import flask
from flask import request, jsonify
from flask import render_template

def get_list_of_regions():
    region_list = []
    ec2 = boto3.client('ec2')
    regions = ec2.describe_regions()
    for region in regions['Regions']:
        region_list.append(region['RegionName'])
    list_of_non_empty_regions = []
    for region in region_list:
        ec2client = boto3.client('ec2', region)
        response = ec2client.describe_instances()
        if response["Reservations"]:
            list_of_non_empty_regions.append(region)
    return(list_of_non_empty_regions)


def get_instances_by_region(region):
    list_of_instances = []
    ec2client = boto3.client('ec2', region)
    response = ec2client.describe_instances()
    for instance in response["Reservations"]:
        list_of_instances.append(instance['Instances'][0]['InstanceId'])
    return(list_of_instances)

def get_instances():
    list_of_instances=[]
    available_regions=get_list_of_regions()
    for region in available_regions:
         ec2client = boto3.client('ec2', region)
         response = ec2client.describe_instances()
         for instance in response["Reservations"]:
             list_of_instances.append(instance['Instances'][0]['InstanceId'])
    return(list(set(list_of_instances)))


def instances_info(instance_id):
    vailable_regions=get_list_of_regions()
    for region in vailable_regions:
        ec2client = boto3.client('ec2', region)
        response = ec2client.describe_instances()
        for instance in response["Reservations"]:
            if instance['Instances'][0]['InstanceId'] == instance_id:
                return(instance['Instances'][0])

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/dashboard', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/regions', methods=['GET'])
def regions():
    return render_template('regions.html', list_of_regions=get_list_of_regions())

@app.route('/instances_by_regions', methods=['GET', 'POST'])
def instances_by_regions():
    if request.method == 'POST':
        zone=request.form['zone']
        instances=get_instances_by_region(zone)
        return render_template('instances_by_regions.html', regions=get_list_of_regions(), zone=zone, instances=instances)
    return render_template('instances_by_regions.html', regions=get_list_of_regions())

@app.route('/instances_details', methods=['GET', 'POST'])
def instances_details():
    if request.method == 'POST':
        instance_id=request.form['instance']
        return render_template('instances_details.html', instances=get_instances(), instance=instance_id, instances_info=instances_info(instance_id))
    return render_template('instances_details.html', instances=get_instances())