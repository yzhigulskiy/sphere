variable "vpc_id" {}
variable "subnets" {}
variable "subnets_cidr" {}
variable "AWS_ACCESS_KEY_ID" {
  type = string
  default = ""
}
variable "AWS_SECRET_ACCESS_KEY" {
  type = string
  default = ""
}

variable "ssh_key" {
  type = string
  default = <<EOF

EOF
}