Terraform
1. Generate ssh keys
  - put public key into file infra/module/ec2/main.tf resource "aws_key_pair" "user" variable "public_key"
  - private key into file infra/dev/variables.tf ssh_key
2. Add AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY into infra/dev/variables.tf to use them during container run as env variable
3. Configure aws: aws configure
4. Run: export AWS_PROFILE
5. Run: terraform apply
6. After this you should be able to get dashboard:
   alb_ip:8080/dashboard

____________________

Docker image available by name yzhigulskiy/sphere:latest on Docker Hub

