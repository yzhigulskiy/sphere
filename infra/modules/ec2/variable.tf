variable "vpc_id" {}
variable "subnets" {}
variable "keypair_private_key" {}
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}