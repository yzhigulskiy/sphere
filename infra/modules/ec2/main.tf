data "template_file" "deploy" {
  template = file("${path.module}/template/deploy.sh")
  vars = {
    "AWS_ACCESS_KEY_ID" = var.AWS_ACCESS_KEY_ID,
    "AWS_SECRET_ACCESS_KEY" = var.AWS_SECRET_ACCESS_KEY
  }
}

resource "aws_key_pair" "user" {
  key_name   = "ec2-user"
  public_key = ""
}

resource "aws_instance" "sphere_aws_instance" {
  instance_type          = "t2.micro"
  ami = "ami-0015b9ef68c77328d"
  key_name               = aws_key_pair.user.key_name
  subnet_id              = var.subnets[0]

  connection {
    type        = "ssh"
    user        = "centos"
    host        = aws_instance.sphere_aws_instance.public_ip
    private_key = var.keypair_private_key
  }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
    encrypted             = true
  }

    provisioner "file" {
    content     = data.template_file.deploy.rendered
    destination = "/tmp/deploy.sh"
  }

    provisioner "remote-exec" {
    inline = [
      "sudo yum install -y -q -e 0 http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-1.el7_6.noarch.rpm",
      "sudo yum install -y yum-utils",
      "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo yum -y install docker-ce docker-ce-cli containerd.io",
      "sudo systemctl start docker"
    ]
  }
}

resource "null_resource" "run_container" {
  depends_on = [aws_instance.sphere_aws_instance]

  triggers = {
    always = timestamp()
  }

  connection {
    type        = "ssh"
    user        = "centos"
    host        = aws_instance.sphere_aws_instance.public_ip
    private_key = var.keypair_private_key
  }

   provisioner "remote-exec" {
    inline = [
      "sudo bash -x /tmp/deploy.sh"
    ]
  }
}
