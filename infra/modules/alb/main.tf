resource "aws_lb" "alb" {
  name                        = var.alb_name
  load_balancer_type          = "application"
  security_groups             = ["${var.security_groups}"]
  subnets                     = var.subnets
  tags = {
    Environment               = terraform.workspace
  }
  access_logs {
    bucket                    = "agi-usea1-1s5pwjeeerbzx"
    enabled                   = true
  }

  idle_timeout                                  = 60
 # internal                                      = true
  enable_deletion_protection                    = false

}

resource "aws_lb_listener" "alb_listener_8080" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type             = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "default backend"
      status_code  = "200"
    }
  }
}

resource "aws_lb_target_group" "lb_target_group_sphere" {
  name                         = "${terraform.workspace}-tg-sphere"
  port                         = "8080"
  protocol                     = "HTTP"
  vpc_id                       = var.vpc_id
  target_type                  = "instance"
  health_check {
    path = "/dashboard"
  }
}

resource "aws_lb_listener_rule" "alb_listener_rule_sphere" {
  listener_arn                 = aws_lb_listener.alb_listener_8080.arn
  priority                     = 4

  action {
    type                       = "forward"
    target_group_arn           = aws_lb_target_group.lb_target_group_sphere.arn
  }

  condition {
    source_ip {
      values                   = ["0.0.0.0/0"]
    }
  }
}


resource "aws_lb_target_group_attachment" "sphere" {
  target_group_arn = aws_lb_target_group.lb_target_group_sphere.arn
  target_id        = var.instance_id
  port             = 8080
}